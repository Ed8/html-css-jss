'use strict';

var gulp = require('gulp');

var sass = require('gulp-sass');

var watch = require('gulp-watch');

var browserSync = require('browser-sync').create();

var concat = require('gulp-concat');

var uglify = require('gulp-uglify');
var pump = require('pump');

var htmlmin = require('gulp-htmlmin');

/* var cleanCSS = require('gulp-clean-css'); */
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');

gulp.task('sass', function() {
	return gulp.src('./src/scss/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('./src/css'))
    .pipe(browserSync.reload({
		stream: true
	}));
});

gulp.task('watch',['browser-sync','sass','concatJs'], function () { 
    gulp.watch('./src/scss/**/*.scss',['sass']);
    gulp.watch('./src/javascript/*.js',['concatJs']);
    gulp.watch('./src/*.html', browserSync.reload);
    gulp.watch('./src/javascript/*.js', browserSync.reload);
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./src"
        }
    });
});

gulp.task('concatJs', function() {
	return gulp.src(['./src/javascript/libs/jquery-3.2.1.min.js','./src/javascript/*.js'])
    .pipe(concat('production.js'))
    .pipe(gulp.dest('./src/js'));
});

gulp.task('minifyJs', function (cb) {
  pump([
        gulp.src('src/js/production.js'),
        uglify(),
        gulp.dest('dist/js/')
    ],
    cb
  );
})

gulp.task('minifyCss', function () {
    gulp.src('./src/css/main.css')
    .pipe(cssmin())
    /*.pipe(rename({suffix: '.min'}))*/
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('minifyHtml', function() {
    return gulp.src('src/*.html')
    .pipe(htmlmin({collapseWhitespace :true}))
    .pipe(gulp.dest('dist'));
});

gulp.task('copy', function(){
    gulp.src('./src/assets/img/*.*')
    .pipe(gulp.dest('./dist/assets/img'));
    gulp.src('./src/fonts/**/*.*')
    .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('build', ['minifyJs','minifyCss','minifyHtml','copy']);